﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Queue
{
    class Program
    {



        public class Queue<T>
        {
            private T[] items;
            private int front;
            private int rear;
            private int capacity;
            /// <summary>
            /// Constructor
            /// </summary>
            public Queue()
            {
                this.capacity = 1000;
                this.items = new T[this.capacity];
                this.front = 0;
                this.rear = 0;
            }
            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="capacity"></param>
            public Queue(int capacity)
            {
                this.capacity = capacity;
                this.items = new T[capacity];
                this.front = 0;
                this.rear = 0;
            }
            /// <summary>
            /// Add item to the Queue
            /// </summary>
            /// <param name="item"></param>
            public void Push(T item)
            {


                if (IsFull())
                {
                    throw new Exception("Queue is full");
                }


                // insert element at the rear 
                items[rear] = item;
                rear++;


            }
            /// <summary>
            /// Returns the first item with deleting
            /// </summary>
            /// <returns></returns>
            public T Pop()
            {

                // check if queue is empty 
                if (IsEmpty())
                {
                    throw new Exception("Queue is empty");
                }
                T frontItem = items[this.front];
                // shift elements to the right
                for (int i = 0; i < rear - 1; i++)
                {
                    items[i] = items[i + 1];
                }
                rear--;
                return frontItem;
            }
            /// <summary>
            ///  Views the first element in the Queue but does not remove it.
            /// </summary>
            /// <returns></returns>
            public T Peek()
            {
                return items[this.front];
            }
            /// <summary>
            /// Contains value in Queue
            /// </summary>
            /// <param name="item"></param>
            /// <returns></returns>
            public bool Contains(T item)
            {
                for (int i = front; i < rear; i++)
                {
                    if (item.Equals(items[i]))
                    {
                        return true;
                    }
                }
                return false;
            }


            /// <summary>
            /// Returns size
            /// </summary>
            /// <returns></returns>
            public int Size()
            {
                return rear;
            }


            /// <summary>
            /// Checks if Queue is empty
            /// </summary>
            /// <returns></returns>
            public bool IsEmpty()
            {
                return front == rear;
            }


            /// <summary>
            /// Checks if Queue is full
            /// </summary>
            /// <returns></returns>
            public bool IsFull()
            {
                return capacity == rear;
            }


            /// <summary>
            /// Reverse Queue
            /// </summary>
            public void Reverse()
            {
                T[] itemsTemp = new T[rear];
                int counter = rear - 1;
                for (int i = front; i < rear; i++)
                {
                    itemsTemp[counter] = items[i];
                    counter--;
                }


                items = itemsTemp;
            }
            static void FrontToLast(Queue<int> q,
                        int qsize)
            {
                // Base condition
                if (qsize <= 0)
                    return;

                // pop front element and push
                // this last in a queue
                q.Push(q.Peek());
                q.Pop();

                // Recursive call for pushing element
                FrontToLast(q, qsize - 1);
            }

            // Function to push an element in the queue
            // while maintaining the sorted order
            public void pushInQueue(Queue<int> q,
                                    int temp, int qsize)
            {

                // Base condition
                if (q.IsEmpty() || qsize == 0)
                {
                    q.Push(temp);
                    return;
                }

                // If current element is less than
                // the element at the front
                else if (temp <= q.Peek())
                {

                    // Call stack with front of queue
                    q.Push(temp);

                    // Recursive call for inserting a front
                    // element of the queue to the last
                    FrontToLast(q, qsize);
                }
                else
                {

                    // Push front element into
                    // last in a queue
                    q.Push(q.Peek());
                    q.Pop();

                    // Recursive call for pushing
                    // element in a queue
                    pushInQueue(q, temp, qsize - 1);
                }
            }



            /*  /// <summary>
              /// sorting
              /// </summary>
              /// <param name="q"></param>
              /// 
              public void sortQueue(Queue<int> q)
              {
                //  Queue<int> q;

                  // Return if queue is empty
                  if (q.IsEmpty())
                      return;

                  // Get the front element which will
                  // be stored in this variable
                  // throughout the recursion stack
                  int temp = q.Peek();

                  // Remove the front element
                  q.Pop();

                  // Recursive call
                  sortQueue(q);

                  // Push the current element into the queue
                  // according to the sorting order
                  pushInQueue(q, temp, q.Size());
              }
              */

            /// <summary>
            /// Print Queue
            /// </summary>
            public void Print()
            {
                if (IsEmpty())
                {
                    throw new Exception("Queue is empty");
                }


                Console.WriteLine("Items in the Queue are:");
                // traverse front to rear and print elements 
                for (int i = front; i < rear; i++)
                {
                    Console.WriteLine(items[i]);
                }
            }


            public   void sortQueue(Queue<int> q)
            {
                //  Queue<int> q;

                // Return if queue is empty
                if (q.IsEmpty())
                    return;

                // Get the front element which will
                // be stored in this variable
                // throughout the recursion stack
                int temp = q.Peek();

                // Remove the front element
                q.Pop();

                // Recursive call
                sortQueue(q);

                // Push the current element into the queue
                // according to the sorting order
                pushInQueue(q, temp, q.Size());
            }
           public int mid()
            {
                int l = rear;
                // Console.WriteLine("Middle element is ", items[l / 2]);

                
                // Console.WriteLine("Middle element is ", items[l / 2]);
                int t = Convert.ToInt16(items[l / 2]);
                return t;
            }
        }
        class QueueIterator<T>
        {
            private Queue<T> currentQueue;


            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="currentQueue"></param>
            public QueueIterator(Queue<T> currentQueue)
            {
                this.currentQueue = currentQueue;
            }


            public bool IsEmpty()
            {
                return this.currentQueue.IsEmpty();
            }




            public T Pop()
            {
                return this.currentQueue.Pop();
            }
        }
        static void Main(string[] args)
        {


            try
            {
             Queue<int> myQueue = new Queue<int>(7);
                myQueue.Push(88);
                myQueue.Push(78);
                myQueue.Push(89);
                myQueue.Push(23);
                myQueue.Push(15);
                myQueue.Push(24);
                myQueue.Push(35);
                myQueue.Print();
                Console.WriteLine("Queue size is: {0}", myQueue.Size());
                myQueue.Reverse();
                Console.WriteLine("Reverse");
                myQueue.Print();
               // Console.WriteLine("sorted queue");
                 Queue<int> Que= new Queue<int>();
               // Que = sortQueue(Que);
                myQueue.Print();
                Console.WriteLine("middle element of the queue is {0}",myQueue.mid());
                //myQueue.mid();


                if (myQueue.Contains(25))
                {
                    Console.WriteLine("Queue contains item 25");
                }
                else
                {
                    Console.WriteLine("Queue does not contain item 25");
                }


                if (myQueue.Contains(15))
                {
                    Console.WriteLine("Queue contains item 15");
                }
                else
                {
                    Console.WriteLine("Queue does not contain item 15");
                }
                Console.WriteLine("The front item is {0}", myQueue.Peek());
                Console.WriteLine("Delete the front item: {0}", myQueue.Pop());
                Console.WriteLine("Queue size is: {0}", myQueue.Size());
                QueueIterator<int> QueueIterator = new QueueIterator<int>(myQueue);
                Console.WriteLine("\nDisplay items using QueueIterator");
                while (!QueueIterator.IsEmpty())
                {
                    Console.WriteLine(QueueIterator.Pop());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();
        }
    }
}
