﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.Threading.Tasks;

namespace final.Exercises
{
    class Exercise15
    {
        public static void Run()
        {
            ObservableCollection<string> hpBooksCollection = new ObservableCollection<string>
            {
                "Philosofer Stone",
                "Chamber of Secrets",
                "Prizon of Azkaban"
            };
            hpBooksCollection.CollectionChanged += hpBooksCollection_CollectionChanged;
            Console.WriteLine();
            var BooktoAdd = "Goblet of Fire";
            Console.WriteLine("Element {0} is added in collection", BooktoAdd);
            hpBooksCollection.Add("Goblet of Fire");
            Console.WriteLine();
            var BooktoRemove = "Chamber of Secrets";
            Console.WriteLine("Element {0} is removed from the collection", BooktoRemove);
            hpBooksCollection.Remove("Chamber of Secrets");
            foreach (var hpBook in hpBooksCollection)
            {
                Console.WriteLine(hpBook);
            }
        }
        private static void hpBooksCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //
        }
    }


}
