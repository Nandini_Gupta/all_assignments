﻿using System;
using System.Collections.Generic;
using System.Text;

namespace final.Exercises
{
    class MyCustomException : Exception
    {
        public MyCustomException(string msg) : base(msg) { }
    }
    class Exercise17
    {
        public static bool validateUserInput(int userInput, int minValue, int maxValue)
        {
            if ((userInput < minValue) || (userInput > maxValue))
            {
                throw new MyCustomException($"Error: Enter a number between {minValue} and {maxValue}");
            }
            return true;
        }

        public static bool checkCondition(int condition)
        {
            int flag = 0, number;
            string userInput;
            userInput = Console.ReadLine();
            if (!(int.TryParse(userInput, out number)))
            {
                throw new MyCustomException($"Exception: Enter any number from {int.MinValue} to {int.MaxValue}");
            }
            else
            {

                if (validateUserInput(number, int.MinValue, int.MaxValue))
                {
                    if (condition == 1)
                    {
                        if (number % 2 == 0)
                        {
                            flag = 1;
                        }
                    }
                    else if (condition == 2)
                    {
                        if (number % 2 == 1)
                        {
                            flag = 1;
                        }
                    }
                    else if (condition == 3)
                    {
                        for (int i = 2; i <= number / 2; i++)
                        {
                            if (number % i == 0)
                            {
                                flag = 1;
                            }
                        }
                    }
                    else if (condition == 4)
                    {
                        if (number < 0)
                        {
                            flag = 1;
                        }
                    }
                    else if (condition == 5)
                    {
                        if (number == 0)
                        {
                            flag = 1;
                        }
                    }
                }

                if (flag == 1)
                {
                    Console.WriteLine("Success");
                    Console.WriteLine();
                    return true;
                }

                Console.WriteLine("Error");
                Console.WriteLine();
                return false;
            }
        }


        public static void Run()
        {
            int number;
            string inputValue;
            int gamesPlayed = 0;
            while (gamesPlayed < 5)
            {
                Console.WriteLine("Enter any number from 1-5");
                inputValue = Console.ReadLine();
                gamesPlayed++;
                try
                {
                    int.TryParse(inputValue, out number);

                    if (validateUserInput(number, 1, 5))
                    {
                        if (number == 1)
                        {
                            Console.WriteLine("Enter even number");
                        }
                        else if (number == 2)
                        {
                            Console.WriteLine("Enter odd number");
                        }
                        else if (number == 3)
                        {
                            Console.WriteLine("Enter prime number");
                        }
                        else if (number == 4)
                        {
                            Console.WriteLine("Enter a negative number");
                        }
                        else
                        {
                            Console.WriteLine("Enter zero");
                        }
                        checkCondition(number);
                    }
                }
                catch (MyCustomException exceptionError)
                {
                    Console.WriteLine(exceptionError.Message);
                }
                Console.WriteLine();
            }
            Console.WriteLine("You have played this game for 5 times");
        }
    }
}
