﻿using System;
using System.Collections.Generic;
using System.Text;

namespace final.Exercises
{
    class Exercise1
    {
        public static void Run()
        {

            Console.WriteLine("Enter Choice:\n 1 for Integer\n 2 for float\n 3 for Boolean");
            int choice = Convert.ToInt32(Console.ReadLine());

            switch(choice)
            {
                case 1:

                    Console.WriteLine("Enter the String to convert into Integer");
                    string stringInput1 = Console.ReadLine();
                    bool int_convert3 = int.TryParse(stringInput1, out Int32 i);
                    int int_convert1 = int.Parse(stringInput1);
                    int int_convert2 = Convert.ToInt32(stringInput1);

                    Console.WriteLine(int_convert1 + " " + int_convert2 + " " + int_convert3);
                    break;

                case 2:

                    Console.WriteLine("Enter the String to convert into Float");
                    string stringInput2 = Console.ReadLine();
                    bool float_convert = float.TryParse(stringInput2, out Single res);
                    float float_convert1 = Convert.ToSingle(stringInput2);
                    float float_convert2 = float.Parse(stringInput2);
                    Console.WriteLine(float_convert + " " + float_convert1 + " " + float_convert2);
                    break;

                case 3:

                    Console.WriteLine("Enter the String to convert into Boolean");
                    string stringInput3 = Console.ReadLine();
                    bool bool_convert = bool.TryParse(stringInput3, out Boolean res1);
                    Console.WriteLine(bool_convert);
                    break;
            }
            
            

        }
    }
}
