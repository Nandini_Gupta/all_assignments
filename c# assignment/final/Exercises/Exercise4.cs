﻿using System;
using System.Collections.Generic;
using System.Text;

namespace final.Exercises
{
    class Exercise4
    {
        class Equipment
        {
            public string equipName { get; set; }
            public string equipDescription { get; set; }
            public int distMoved { get; set; }
            public int maintainanceCost { get; set; }

        public void CreateObject()
            {
                Console.WriteLine("Create an Object:");
                Console.WriteLine("Enter Equipment Name: ");
                equipName = Console.ReadLine();
                Console.WriteLine("Enter Equipment Description: ");
                equipDescription = Console.ReadLine();
                Console.WriteLine("Enter Equipment Distance Moved Till Date: ");
                distMoved = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter Equipment Maintainance Cost: ");
                maintainanceCost = Convert.ToInt32(Console.ReadLine());
            }
        public void ShowDetails()
            {
                Console.WriteLine(equipName);
                Console.WriteLine(equipDescription);
                Console.WriteLine(distMoved);
                Console.WriteLine(maintainanceCost);

            }

        }
        class MobileEquipment : Equipment
        {
            public int numOfWheels { get; set; }

            public void MoveBy(int dist)
            {
                distMoved += dist;
                maintainanceCost += numOfWheels * dist;
            }
        }

        class ImmobileEquipment : Equipment
        {
            public int weight { get; set; }

            public void MoveBy(int dist)
            {
                distMoved += dist;
                maintainanceCost += weight * dist;
            }
        }
        public static void Run()
        {
            MobileEquipment mbEquip = new MobileEquipment();
            ImmobileEquipment immbEquip = new ImmobileEquipment();
            int userChoice = 4;
            while (userChoice != 0)
            {
             Console.WriteLine("Enter the operation:\n1: Create Object\n2: Move By\n3: Show Details\n Enter 0 to Exit");
             userChoice= Convert.ToInt32(Console.ReadLine());
            
                switch (userChoice)
                {
                    case 1:
                        Console.WriteLine("Select Equipment Type:\n1. Mobile\n2. Immobile");
                        int equipType = Convert.ToInt32(Console.ReadLine());
                        if (equipType == 1)
                        {
                
                            mbEquip.CreateObject();
                            Console.WriteLine("Enter The number of Wheels: ");
                            mbEquip.numOfWheels = Convert.ToInt32(Console.ReadLine());
                            
                        }
                        else if (equipType == 2)
                        {
                            
                            immbEquip.CreateObject();
                            Console.WriteLine("Enter Weight of equipment");
                            immbEquip.weight = Convert.ToInt32(Console.ReadLine());
                        }

                        break;
                    case 2:

                        Console.WriteLine("Select Equipment Type:\n1. Mobile\n2. Immobile");
                        int equipType1 = Convert.ToInt32(Console.ReadLine());

                        if (equipType1==1)
                        {
                            Console.WriteLine("Enter the distance to move the euipment:");
                            int distMoveBy = Convert.ToInt32(Console.ReadLine());
                            mbEquip.MoveBy(distMoveBy);

                        }
                        else if (equipType1 == 2)
                        {
                            Console.WriteLine("Enter the distance to move the euipment:");
                            int distMoveBy = Convert.ToInt32(Console.ReadLine());
                            immbEquip.MoveBy(distMoveBy);

                        }

                        break;

                    case 3:

                        Console.WriteLine("Select Equipment Type:\n1. Mobile\n2. Immobile");
                        int equipType2 = Convert.ToInt32(Console.ReadLine());

                        if (equipType2 == 1)
                        {
                            mbEquip.ShowDetails();
                        }
                        else if (equipType2 == 2)
                        {
                            immbEquip.ShowDetails();

                        }
                        break;
                }

            }
            

        }
    }
}
