﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace final.Exercises
{
    class Exercise7
    {
        enum DuckType { Rubber, Mallard, Redhead };
        public interface IShowDetails
        {
            void ShowDetails();
        }

        class Duck : IShowDetails
        {
            public double wings;
            public double weight;
            public DuckType duckType;

            public Duck() { }
            public Duck(double wings, double weight, DuckType duckType)
            {
                this.wings = wings;
                this.weight = weight;
                this.duckType = duckType;
            }

            public virtual void ShowDetails()
            {
                if (!(duckType == DuckType.Rubber || (duckType == DuckType.Mallard) || (duckType == DuckType.Redhead)))
                {
                    Console.WriteLine("Please provide the correct duck type");
                }
                Console.WriteLine($"Wings of the duck: {wings}");
                Console.WriteLine($"Weight of the duck: {weight}");
            }
        }

        class RubberDuck : Duck
        {
            public RubberDuck() { }
            public RubberDuck(double wings, double weight, DuckType duckType) : base(wings, weight, duckType) //Using base class variables
            {

            }
            public override void ShowDetails()
            {
                Console.WriteLine("It is a Rubber duck");
                base.ShowDetails();                            //Here base keyword represents the base class i.e Duck 
                Console.WriteLine("Rubber ducks don't fly and squeak.");
            }
        }

        class MallardDuck : Duck
        {
            public MallardDuck() { }
            public MallardDuck(double wings, double weight, DuckType duckType) : base(wings, weight, duckType)
            {

            }
            public override void ShowDetails()
            {
                Console.WriteLine("It is a Mallard duck");
                base.ShowDetails();
                Console.WriteLine("Mallard ducks fly fast and quack loud.");
            }
        }


        class RedheadDuck : Duck
        {
            public RedheadDuck() { }
            public RedheadDuck(double wings, double weight, DuckType duckType) : base(wings, weight, duckType)
            {

            }
            public override void ShowDetails()
            {
                Console.WriteLine("It is a Redhead duck");
                base.ShowDetails();
                Console.WriteLine("Redhead ducks fly slow and quack mild.");
            }
        }

        public static void Run()
        {
            List<IShowDetails> ducks = new List<IShowDetails>();
            int userChoice = 6, userInput;

            while (userChoice != 0)
            {
                Console.WriteLine("1. Add a duck\n" +
                    "2. Remove a duck\n" +
                    "3. Remove all ducks\n" +
                    "4. List of ducks in the increasing order of their weights\n" +
                    "5. List of ducks in the increasing order of number of wings\n" +
                    "0. Enter 0 to exit");
                string input = Console.ReadLine();
                if (int.TryParse(input, out userInput))
                {
                    switch (userInput)
                    {
                        case 0:
                            userChoice = 0;
                            break;
                        case 1:
                            Console.WriteLine("Add a duck");
                            if (AddDuck(ducks) == true)
                            {
                                Console.WriteLine("Addition Successful");
                            }
                            else
                            {
                                Console.WriteLine("Addition Unsuccessful");
                            }
                            break;
                        case 2:
                            Console.WriteLine("Remove a duck");
                            if (RemoveDuck(ducks))
                            {
                                Console.WriteLine("Removal of duck successful");
                            }
                            else
                            {
                                Console.WriteLine("Removal of duck unsuccessful");
                            }
                            break;
                        case 3:
                            ducks.Clear();
                            Console.WriteLine("Removal of all ducks successful");
                            break;
                        case 4:
                            Console.WriteLine("Increasing order of their weights");
                            IncreasingOrderOfWeight(ducks);
                            break;
                        case 5:
                            Console.WriteLine("Increasing order of number of wings");
                            IncreasingOrderOfWings(ducks);
                            break;
                        default:
                            Console.WriteLine("Please enter a valid choice");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Invalid input");
                }
                Console.WriteLine();
            }
        }

        public static bool IncreasingOrderOfWeight(List<IShowDetails> ducks)
        {
            var orderByWeight = from equipment in ducks
                                orderby ((Duck)equipment).weight
                                select equipment;

            foreach (var equipment in orderByWeight)
            {
                Console.WriteLine(((Duck)equipment).weight);
            }
            return true;
        }

        public static bool IncreasingOrderOfWings(List<IShowDetails> ducks)
        {
            var orderByWings = from equipment in ducks
                               orderby ((Duck)equipment).wings
                               select equipment;

            foreach (var equipment in orderByWings)
            {
                Console.WriteLine(((Duck)equipment).wings);
            }
            return true;
        }

        public static bool RemoveDuck(List<IShowDetails> ducks)
        {
            Console.WriteLine("Enter the duck number you want to remove");
            string inputDuckNumber = Console.ReadLine();
            if (int.TryParse(inputDuckNumber, out int duckNumber))
            {
                if (ducks.Count < duckNumber || duckNumber < 1)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

            for (int i = 0; i < ducks.Count; i++)
            {
                if (i + 1 == duckNumber)
                {
                    ducks.RemoveAt(i);
                    return true;
                }
            }
            return false;
        }
        public static bool AddDuck(List<IShowDetails> ducks)
        {
            Console.WriteLine("Enter the type of duck\n1. Rubber Duck\n2. Mallard Duck \n3. Redhead Duck");
            string inputDuckType = Console.ReadLine();
            int.TryParse(inputDuckType, out int duckType);
            Console.WriteLine(duckType);
            if (duckType < 1 || duckType > 3)
            {
                Console.WriteLine("Enter a valid duck type");
                return false;
            }

            Console.WriteLine("Enter the number of wings: ");
            string inputWing = Console.ReadLine();
            if (!(int.TryParse(inputWing, out int wings)))
            {
                Console.WriteLine("Invalid number of wings");
                return false;
            }

            Console.WriteLine("Enter the weight: ");
            string inputWeight = Console.ReadLine();
            if (!(int.TryParse(inputWeight, out int weight)))
            {
                Console.WriteLine("Invalid weight");
                return false;
            }

            if (duckType == 1)
            {
                ducks.Add(new RubberDuck(wings, weight, DuckType.Rubber));
            }
            else if (duckType == 2)
            {
                ducks.Add(new MallardDuck(wings, weight, DuckType.Mallard));
            }
            else if (duckType == 3)
            {
                ducks.Add(new RedheadDuck(wings, weight, DuckType.Redhead));
            }

            return true;
        }
    }
}
