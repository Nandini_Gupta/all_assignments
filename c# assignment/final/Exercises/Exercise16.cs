﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace final.Exercises
{
    class Exercise16
    {
        public static void Run()
        {
            Console.WriteLine("Enter the path of directory");
            string pathOfDirectory = Console.ReadLine();
            if (!(Directory.Exists(pathOfDirectory)))
            {
                Console.WriteLine("The given path of directory does not exists.");
            }
            else
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(pathOfDirectory);


                //1) 
                FileInfo[] textFiles = directoryInfo.GetFiles("*.txt", SearchOption.TopDirectoryOnly);
                int noOfTextFiles = textFiles.Length;
                Console.WriteLine($"Number of text files in the directory: {noOfTextFiles} ");
                Console.WriteLine();


                //2)
                FileInfo[] allFiles = directoryInfo.GetFiles("*", SearchOption.TopDirectoryOnly);

                //key of the dictionary represents extension type and value represents no of files of that extension type
                Dictionary<string, int> perExtensionType = new Dictionary<string, int>();
                string extension = "";
                for (int i = 0; i < allFiles.Length; i++)
                {
                    extension = allFiles[i].Extension;
                    if (perExtensionType.ContainsKey(extension))
                    {
                        perExtensionType[extension] += 1;
                    }
                    else
                    {
                        perExtensionType[extension] = 1;
                    }
                }

                foreach (KeyValuePair<string, int> entry in perExtensionType)
                {
                    Console.WriteLine($"Number of files with extension type {entry.Key}: {entry.Value}");
                }
                Console.WriteLine();


                //3)
                List<long> topFiveFiles = new List<long>();

                for (int i = 0; i < allFiles.Length; i++)  //Adding the file sizes to a list topFiveFiles
                {
                    topFiveFiles.Add(allFiles[i].Length);
                }


                topFiveFiles.Sort();
                topFiveFiles.Reverse();


                for (int i = 0; i < (topFiveFiles.Count < 5 ? topFiveFiles.Count : 5); i++)
                {
                    for (int j = 0; j < allFiles.Length; j++)
                        if (topFiveFiles[i] == allFiles[j].Length)
                        {
                            Console.WriteLine($"Size of {allFiles[j]}: {topFiveFiles[i]}");
                            break;
                        }
                }
                Console.WriteLine();


                //4) 
                if (topFiveFiles.Count > 0)
                {
                    for (int i = 0; i < allFiles.Length; i++)
                    {
                        if (allFiles[i].Length == topFiveFiles[0])
                        {
                            Console.WriteLine($"{allFiles[i]} has the maximum length of {topFiveFiles[0]}");
                            break;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Current directory consists of zero files");
                }
                Console.WriteLine();
            }
        }
    }
}
