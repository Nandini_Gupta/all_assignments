﻿using System;
using System.Collections.Generic;
using System.Text;

namespace final.Exercises
{
    class Exercise2
    {
        public static void Run()
        {
            string firstName = "Nandini";
            string myName = firstName;
            string lastName = "Gupta";
            ///Compare reference of the Objects.
            Console.WriteLine("== operator{0}", firstName == myName);

            ///Same Reference
            Console.WriteLine("Equals method is {0}", object.Equals(firstName, myName));

            ///contents will be the same in both object variables but both have different references.
            Console.WriteLine("Equals method is {0}", object.ReferenceEquals(lastName, myName));
        }
    }
}
