﻿using System;
using System.Collections.Generic;
using System.Text;


using System.Linq;

using System.Threading.Tasks;

namespace final.Exercises
{
    public static class ExtensionMethod
    {
        /// <summary>
        /// Returns true if number is odd.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static bool IsOdd(this int number)
        {
            return (number % 2 == 0) ? false : true;
        }

        /// <summary>
        /// Returns true if number is even.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static bool IsEven(this int number)
        {
            return (number % 2 == 0) ? true : false;
        }

        /// <summary>
        /// Returns true if number is prime.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static bool IsPrime(this int number)
        {
            int i;
            for (i = 2; i < number; i++)
            {
                if (number % i == 0)
                {
                    break;
                }
            }
            return (i == number) ? true : false;
        }

        /// <summary>
        /// Returns True if number is divisible by the given number.
        /// </summary>
        /// <param name="number"></param>
        /// <param name="divisible"></param>
        /// <returns></returns>
        public static bool IsDivisibleBy(this int number, int divisible)
        {
            return (number % divisible == 0) ? true : false;
        }
    }
    class Exercise11
    {
        public static void OddNum(int number)
        {
            bool oddnum = number.IsOdd();
            Console.WriteLine("Odd Number - " + oddnum);
        }

        /// <summary>
        /// Function to make a check for even number
        /// </summary>
        /// <param name="number"></param>
        public static void EvenNum(int number)
        {
            bool evennum = number.IsEven();
            Console.WriteLine("Even Number - " + evennum);
        }

        /// <summary>
        /// Function to make a check for Prime number
        /// </summary>
        /// <param name="number"></param>
        public static void PrimeNum(int number)
        {
            bool primenum = number.IsPrime();
            Console.WriteLine("Prime Number - " + primenum);
        }

        /// <summary>
        /// Function to make a check is number is divisible by another number.
        /// </summary>
        /// <param name="number"></param>
        public static void IsDivisibleBy(int number)
        {
            Console.WriteLine("Enter Number to Check Divisiblity:");
            int divisible = int.Parse(Console.ReadLine());
            bool divisiblenum = number.IsDivisibleBy(divisible);
            Console.WriteLine("Is Divisible By " + divisible + " : " + divisiblenum);
        }

        public static void Run()
        {
            Console.WriteLine("Enter Number:");
            var number = int.Parse(Console.ReadLine());

            OddNum(number);

            EvenNum(number);

            PrimeNum(number);

            IsDivisibleBy(number);
        }
    }
}
