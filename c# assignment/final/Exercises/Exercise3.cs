﻿using System;
using System.Collections.Generic;
using System.Text;

namespace final.Exercises
{
    class Exercise3
    {
        public static void Run()
        {
            Exercise3.UserInput();
        }
        public static void FindPrimeNumber(int   x, int y)
        {
            int number, iterVar, flag;
            Console.WriteLine("prime function working");
            for (number = x; number <= y; number++)
            {
                flag = 0;

                for (iterVar = 2; iterVar <= number / 2; iterVar++)
                {
                    if (number % iterVar == 0)
                    {
                        flag++;
                        break;
                    }
                }

                if (flag == 0 && number != 1)
                    Console.Write("{0} \t", number);
            }
        }
        public static void UserInput()
        {
            Console.WriteLine("Enter the First Number");
            int startingNumber = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the Second Number");
            int endNumber = Convert.ToInt32(Console.ReadLine());

            if (endNumber > startingNumber)
            {
                Exercise3.FindPrimeNumber(startingNumber, endNumber);  
            }

            else
            {
                Console.WriteLine("Error:: enter the numbers correctly");
                Exercise3.UserInput();
            }

        }
    }
}
