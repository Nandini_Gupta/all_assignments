﻿using System;
using final.Exercises;

namespace final
{
    class Program
    {
        static void Main(string[] args)
        {
            int c = 18;
            while (c != 0)
            {
                Console.WriteLine("Enter the Exercise Number (1-17):");
                c = Convert.ToInt32(Console.ReadLine());
                switch (c)
                {
                    case 0:
                        break;
                    case 1:
                        Exercise1.Run();
                        break;

                    case 2:
                        Exercise2.Run();
                        break;

                    case 3:
                        Exercise3.Run();
                        break;

                    case 4:
                        Exercise4.Run();
                        break;

                    case 5:
                        Exercise5.Run();
                        break;

                    case 6:
                        Exercise6.Run();
                        break;

                    case 7:
                        Exercise7.Run();
                        break;

                    case 9:
                        Console.WriteLine("----Incomplete----");
                        //Exercise9.Run();
                        break;
                    case 10:
                        Console.WriteLine("----Incomplete----");
                        //Exercise10.Run();
                        break;
                    case 8:
                        Console.WriteLine("----Incomplete----");
                        //Exercise8.Run();
                        break;

                    case 11:
                        Exercise11.Run();
                        break;
                    case 12:
                        Exercise12.Run();
                        break;
                    case 13:
                        Exercise13.Run();
                        break;
                    case 14:
                        Exercise14.Run();
                        break;
                    case 15:
                        Exercise15.Run();
                        break;
                    case 16:
                        Exercise16.Run();
                        break;
                    case 17:
                        Exercise17.Run();
                        break;

                    default:
                        Console.WriteLine("Enter Valid Exercise Number");
                        break;

                }
                Console.WriteLine("\n\n");
            }

            if (c == 0)
            {
                Console.WriteLine("Exiting the Assignment");
            }



        }
    }
}

        
    

